# PowerShell Check_mk Installer (von Remote Check_mk Client installer)
# Version 0.0 (Potato Version)
# Chris Wolfensberger
# April-Mai 2014
# Exec n' drink Coffee

#
#	Conf
####################################################################################################
# Dear Admin, edit the following Line!
$ServerIP="10.250.254.20"	# !!!!
$ConfFile=""

#
#	Installation
####################################################################################################
echo "Starte Client Installation!"
if(Test-Path "./chmk_agent.exe"){
	& './chmk_agent.exe  /S'
}else{
	echo "File [./chmk_agent.exe] konnte nicht gefunden werden. Evtl. in der anderen Hosentasche?"
	exit 1
}

#
#	Konfiguration
####################################################################################################
if(Test-Path $ConfFile){
	echo "Passe die Konfiguration an...."
	#(gc $ConfFile) -replace '#only_from      = 127.0.0.1','only_from      = $ServerIP' | Set-Content $ConfFile	# I don't trust this Command >.<
}else{
	echo "File [$ConfFile] konnte nicht gefunden werden. Benötige ein Navi."
	exit 1
}


#
#	Firewall
####################################################################################################
new-netfirewallrule -name Check_mkClient -displayname Check_mkClient -localport 6556 -profile any -action allow -protocol tcp

#
#	Check_mk Service neustarten....
####################################################################################################
Restart-Service check_mk_agent