#!/bin/bash

# Linux Check_mk Installer (von Remote Check_mk Client installer)
# Version 0.0.93267.8482.31290.0a
# Chris Wolfensberge
# April-Mai 2014
# Exec n' drink Coffee

#
#	Conf
####################################################################################################

# Dear Admin, edit the following Line!
ServerIP="10.250.254.20"	# !!!!
IPForwarding=1			# Get the IP from the rci Script. 1=Enable | 2=Disable

# PacketTools
zypper=`which zypper 2> /dev/null`
yast=`which yast 2> /dev/null`
xinetd=`which xinetd 2> /dev/null`
dpkg=`which dpkg 2> /dev/null`

# Check_mk Agents und Script Links:
cmkrpm="http://$ServerIP/download/chmk_agent.rpm" 	# Check_mk Installer
cmkdeb="http://$ServerIP/download/chmk_agent.deb" 	# Apache Config anpassen, dass kein Login benötigt für diese Files
cmkexe="http://$ServerIP/download/chmk_agent.exe"	# Ziemlich hardcoded ....

# Log Settings:
log=1				# 1= Logs  |   2= Keine Logs .....
logfile="./tmp.log"		# Ein Logfile welches mehr Bits benötigt, geben wir ihm mehr!

# Init
installtool="non"
pak="none"
logmsg="none"

#
#	Funktionen
####################################################################################################
function logit() {
	if [ $log -eq 1 ]
	then
		timedate=`date`
		echo "$timedate: $logmsg" >> $logfile
	fi
}
function RemoteIP() {
	ip=$1
	checkIp=`ping -c 1 -W 2 $ip > /dev/null 2>&1; echo $?`
	if [ $checkIp -eq 0 -a $IPForwarding -eq 1]
	then
		ServerIP=$ip
		logmsg="RemoteIP: Set Server IP to $ip."
		echo $logmsg; logit	
	fi
}
#
#	HorseRace
####################################################################################################
# Wenn IPForwarding Enabled ist, kann die IP aus den Argumenten gelesen werden:
RemoteIP

# Ueberpruefe welche Programme für die Packetinstallation zur verfügung stehen
# Kann erweitert werden, z.B.: für debian
if [ -f "$zypper" ]
then
	installtool="zypper"
	logmsg="[$zypper] wurde gefunden."
	echo $logmsg; logit
	if [ ! -f "$xinetd" ]
	then
		$zypper install -l -f -n xinetd
		systemctl enable xinetd
	fi
fi
if [ -f "$dpkg" ]
then
	installtool="dpkg"
	logmsg="[$dpkg] wurde gefunden."
	echo $logmsg; logit
	#if [ ! -f "$xinetd" ]
	#then
	#	$zypper install -l -f -n xinetd
	#	systemctl enable xinetd
	#fi
fi
# Ueberpruefe welche Agents zurverfügung stehen
# erweiterbar für z.B.: .deb Packete
if [ -f ./chmk_agent.rpm ]
then
	pak="rpm"
	logmsg="Packet [./chmk_agent.rpm] wurde gefunden."
	echo $logmsg; logit
fi

# Versuche Pakete nachzuladen, falls nicht vorhanden
# erweiterbar.... wie immer
if [ "$pak" == "none" ]
then
	logmsg="Check_mk Agent nicht gefunden. Versuche ihn nun jetzt zu laden."
	echo $logmsg; logit
	if [ "$installtool" == "zypper" ]
	then
		wget -T 5 $cmkrpm
		if [ $? -eq 0 ]
		then
			pak="rpm"
			logmsg="Check_mk Agent (RPM) wurde geladen."
			echo $logmsg; logit
		else
			logmsg="Fehler: Check_mk Agent (RPM) konnte nicht geladen werden. Ueberpruefe Server IP."
			echo $logmsg; logit
			exit 1
		fi
	fi
fi

# Stelle sicher das Paket nun vorhanden ist
# wehe es ist nicht vorhanden >.<
if [ "$pak" == "none" ]
then
	logmsg="Fehler: Check_mk Agent nicht gefunden."
	echo $logmsg; logit
	exit 1
fi

#
# Client Installation
#
if [ "$installtool" == "zypper" ]
then
	$zypper install ./chmk_agent.rpm
	if [ $? -ne 0 ]
	then
		logmsg="Fehler: Check_mk Agent konnte nicht installiert werden."
		echo $logmsg; logit	
		exit 1	
	fi
	logmsg="Check_mk Agentwurde erfolgreich installiert."
	echo $logmsg; logit
fi
if [ "$installtool" == "dpkg" ]
then
	$dpkg -i ./chmk_agent.deb && apt-get -f install
	if [ $? -ne 0 ]
	then
		logmsg="Fehler: Check_mk Agent konnte nicht installiert werden."
		echo $logmsg; logit	
		exit 1	
	fi
	logmsg="Check_mk Agentwurde erfolgreich installiert."
	echo $logmsg; logit
fi
#
# Client konfiguration
#
old="#only_from      = 127.0.0.1"
new="only_from      = $ServerIP"	# Erlaube Zugriff nur vom MonitoringServer, oder jedem mit der IP-Adresse
					# des Monitoring Servers :) (check_mk Abfrage-Zugriff auf Port 6556)
if [ -f /etc/xinetd.d/check_mk ]
then
	cp /etc/xinetd.d/check_mk /etc/xinetd.d/check_mk.backup # Buckup.... wast of time
	sed 's/$old/$new/g' /etc/xinetd.d/check_mk > /etc/xinetd.d/check_mk
	logmsg="Bearbeite /etc/xinetd.d/check_mk Konfigurationsfile."
	echo $logmsg; logit
	logmsg="Ersetzte [$old] durch [$new]."
	echo $logmsg; logit
	systemctl restart xinetd
else
	logmsg="Das File [/etc/xinetd.d/check_mk] wurde nicht gefunden."
	echo $logmsg; logit
fi

#
# Firewall
#

# OpenSuse open Firewall Port 6556
if [ -f "$yast" ]
then
	echo "Destroy Firewall ...."
	logmsg="OpenSuse's Yast wurde entdeckt. Erstelle Firewall Rule (TCP:6556:ext)."
	echo $logmsg; logit

	yast2 firewall services add tcpport=6556 zone=EXT
	SuSEfirewall2 status > /dev/null 2>&1
	if [ $? -eq 0 ]
	then
		logmsg="Starte FW neu...."
		echo $logmsg; logit
		logmsg="SuSEfirewall2 stop:";logit
		SuSEfirewall2 stop
		logmsg="SuSEfirewall2 start:";logit
		SuSEfirewall2 start
	fi
fi

