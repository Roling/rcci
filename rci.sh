#!/bin/bash

# Remote Check_mk Client installer
# Version 0.0.93267.8482.37490.0a
# Chris Wolfensberger
# April-Mai 2014
# Exec n' drink Coffee

#
#	Conf (Räum ich dan noch auf, versprochen)
####################################################################################################
# System Informationen:
ip=`ifconfig | awk -F':' '/inet addr/&&!/127.0.0.1/{split($2,_," ");print _[1]}'`; # <- ueberpruefen ob dies auf deiner distro funktioniert :)
listfile="./Hostlist"		# File in welchem die Liste der Zielsysteme ist. The SSF/IL! (Space seperated FQDN/IP List)
				# Erlaubt so auch Ausführung durch externe programme, z.B.: einem WUI :)

# Check_mk Agents und Script Links:
cmkrpm="http://$ip/download/chmk_agent.rpm" 	# Check_mk Installer
cmkdeb="http://$ip/download/chmk_agent.deb" 	# Apache Config anpassen, dass kein Login benötigt für diese Files
cmkexe="http://$ip/download/chmk_agent.exe"	# Ziemlich hardcoded ....
wininstall="http://$ip/download/wininstall.ps1"	# Powershell-Script welches den Check_mk Agent installiert
lininstall="http://$ip/download/lininstall.sh"  # Shell-Script welches den Check_mk Agent installiert

#Binary's
winexe="/bin/winexe" 		# OR SO ..... muss zuerst installiert werden
sshpassbin="/path/to/sshpass"	# Pfad zum sshpas, sollte angepasst werden. (Muss ebenfalls zuerst istalliert werden.)

# Log Settings:
log=1				# 1= Logs  |   2= Keine Logs .....
logfile="./tmp.log"		# Ein Logfile welches mehr Bits benötigt, geben wir ihm mehr!

# Vorgehensmodelle:
Hostlisttype="cli"; 		# cli: read hostlist out of CLI input.
				# file: read hostlist out of File.

# Variablen initialisierung:
Hostlist="";
logmsg="None"

#
#	Funktionen
####################################################################################################

# check functions
function CheckBins() {
	breakup=0
	bins="$winexe $sshpassbin"; # <- sollte mit allen verwendeten binarys ergänzt werden

	for bin in $bins
	do
		if [ ! -f $bin ]
		then
			breakup=1
			logmsg="Fehler: Binary [$bin] wurde nicht gefunden."
			echo $logmsg
			logit	# Die Funktion logit (logge es!) schreibt den Inhalt von $logmsg in das File $logfile
		fi		# Wenn die variable log dem Wert 1 entspricht. Nur so als Info...
	done
	if [ $breakup -eq 1 ]
	then
		exit 1
	fi
}

function logit() {
	if [ $log -eq 1 ]
	then
		timedate=`date`
		echo "$timedate: $logmsg" >> $logfile
	fi
}
function OSDetectWithSSH() {
	# Der zweit schlechteste OS-Detector der Welt!
	answer=`echo Surprise | nc -w1 $ClientIP 22 > /dev/null; echo $?`
	if [ $answer -eq 0 ]
	then
		OS="linux"
		logmsg="OS [Linux] wurde beim Host [$ClientIP] erkannt."
		echo $logmsg; logit
	else
		OS="win"
		logmsg="OS [Windows] wurde beim Host [$ClientIP] erkannt."
		echo $logmsg; logit
	fi
}
function OSDetectRandom() {
	# Der schlechteste OS-Detector der Welt!
	answer=`$((RANDOM%1))`
	if [ $answer -eq 0 ]
	then
		OS="linux"
		logmsg="OS [Linux] wurde beim Host [$ClientIP] erkannt. Ich denke aber nicht das dies korrekt ist."
		echo $logmsg; logit
	else
		OS="win"
		logmsg="OS [Windows] wurde beim Host [$ClientIP] erkannt. Ich denke aber nicht das dies korrekt ist."
		echo $logmsg; logit
	fi
}

# Information Funktions
function ReadServerListCLI() {
	echo "Please enter the hole Hostlist (Space Seperated):";
	read $Hostlist;
}
function ReadServerListFile() {
	Hostlist=`cat $listfile`
}
function WinLogin(){
	echo "Enter Windows Loginname:"
	read $winname
	echo "Enter Windows Userpassword:"
	read $winpw
	echo "Enter Windows Domain (or let Empty for no Domain):"
	read $windom
}
function LinuxLogin(){
	echo "Enter Linux Loginname:"
	read $linname
	echo "Enter Linux Userpassword:"
	read $linpw
}
function wuiLoginWin(){
	# WinLogin Funktion falls, das script aus einem WUI oder anderem externem
	# PRogramm aufgerufen wird. ^^
	winname=$1
	winpw=$2
	windom=$3

	# example:
	# Run Script like This:
	# rci.sh LinuxUserName LinuxPW WinUsername WinPW WinDomain
	# or so:
	# rci.sh "root" "secret" "Administrator" "othersecret" "\"
	# exec this function in the script like this:
	# wuiLoginWin $3 $4 $5
	# wuiLoginLinux $1 $2

}
function wuiLoginLinux(){
	# Linux Login Funktion falls, das script aus einem WUI oder anderem externem
	# PRogramm aufgerufen wird. ^^

	linname=$1
	linpw=$2

	# example:
	# Run Script like This:
	# rci.sh LinuxUserName LinuxPW WinUsername WinPW WinDomain
	# or so:
	# rci.sh "root" "secret" "Administrator" "othersecret" "\"
	# exec this function in the script like this:
	# wuiLoginWin $3 $4 $5
	# wuiLoginLinux $1 $2
}


# Activ Funktions
function WinInstall() {
	touch ./rcitmp/$1
	logmsg="### Starte Check_mk Installation auf dem Host [$1]. Host OS [Windows] wurde erkannt."
	echo $logmsg; logit

	if [ "$windom" != "" ]
	then
		$winlogin="-U $windom/$winname%$winpw"
	else
		$winlogin="-U $winname%$winpw"
	fi
	# öffnet jeweils interaktive powershell, mit perl besser scriptbar :/, 
	# Daher wird nun ein Script auf Zielsystem geladen werden, welches die Installation vornimmt.
	# http://monitoring-portal.org/wbb/index.php?page=Thread&postID=202343#post202343 winexe infos
	# http://social.technet.microsoft.com/Forums/de-DE/b78f9f0f-5f0f-4a5f-8c7a-39b5371841c6/powershell-script-remote-von-linux-ausfhren?forum=powershell_de winexe infos

	# Läd das Powershellscript auf Ziel System und führt es aus:
	echo "wget $cmkexe;wget $wininstall; wininstall.ps1;exit" | $winexe $winlogin //$1 'powershell -ExecutionPolicy RemoteSigned -NoExit -Command -'

	rm ./rcitmp/$1
}
function Lininstall() {
	touch ./rcitmp/$1
	logmsg="### Starte Check_mk Installation auf dem Host [$1]. Host OS [Linux] wurde erkannt."
	echo $logmsg; logit

	# sshpass sollte installiert werden, sonst kann es zu dem Fehler der unfunktionalität führen.
	sshpass -p "$linpw" ssh $1 -l $linname -o StrictHostKeyChecking=no "wget $cmkrpm; wget $lininstall;sh lininstall.sh $ip; exit"

	rm ./rcitmp/$1
}

#
# Workflow
####################################################################################################

# Ueberpruefe ob noetige Programme auffindbar sind...
CheckBins

# Cli oder File? cli,file?cli,file,cli,file...CLI ODER FILE?!?!?!
if [ "$Hostlisttype" == "cli" ]
then
	ReadServerListCLI
fi
if [ "$Hostlisttype" == "file" ]
then
	ReadServerListFile
fi

# Start with installllllll
if [ ! -d ./rcilog ]
then
	mkdir ./rcilog
fi
if [ ! -d ./rcitmp ]
then
	mkdir ./rcitmp
fi
rm -r ./rcitmp/*
rm -r ./rcilog/*
timedate=`date`
logmsg="Starte [Remote Check_mk Client installer] um [$timedate]."
echo $logmsg; logit
echo
echo "Willkommen beim Super coolen ClientInstaller für remote hosts blöablabla"
echo "Zuerst möchte ich deine Logins, damit ich sie der NSA weitergeben kan ;)"
WinLogin()
LinuxLogin()

ClientCount=0	
for ClientIP in $Hostlist
do
	ClientCount=$(($ClientCount+1))
	logmsg="Starte Agent installation auf dem Host [$ClientIP]."
	echo $logmsg; logit	
	# Dieser OS Detector ist nicht sehr verlässlich :/
	# Aber ich habe auf die schnelle nichts besseres hingekriegt :'(
	# Sorry
	OSDetectWithSSH

	if [ "$OS" == "win" ]
	then			
		WinInstall $ClientIP > ./rcilog/$ClientIP & # \/
	fi
	if [ "$OS" == "linux" ]
	then	
		Lininstall $ClientIP > ./rcilog/$ClientIP & # \/	
			# Das '&' Könnte eine beschleunigende wirkung haben und die Clients parallel installieren.
	fi		# Jedoch könnte auch ein Chaos ausbrechen.
done			# Einige Physiker diskutieren gerade auch über die Theorie, dass das zu schnelle Ausführen
			# schwarze Löcher auf den Ziel- und dem Hostsystem generiert, welche durch eine art von
			# Hyper-/Subraumverbindung zueinander verbunden sind und so das schnelle Transportieren von
			# Markenjeans erlauben, dieses '&' könnte also theoretisch für das Schmuggeln verwendet werden.
			# Also bei Auftreten von schwarzen Löchern sofort den Zoll kontaktieren!

running=1
logmsg="Alle Installationen wurde gestartet. Warte nun auf Abschluss..."
echo $logmsg; logit
while [ $running -eq 1 ]
do
	files=`ls ./rcitmp/ | wc -w`
	if [ $files -eq 0 ]
	then
		echo "############################ CLIENT OUTPUTS ############################" >> $logfile
		cat ./rcilog/* >> $logfile
		echo "############################ FINISH INSTALL ############################" >> $logfile
		echo
		timedate=`date`
		logmsg="Alle Installationen abgeschlossen um [$timedate]."
		echo $logmsg; logit
		logmsg="Bitte ueberpruefe das [$logfile] Logfile."
		echo $logmsg; logit

		echo "Archivire ClientInstallations-Logs...."
		tar -zcvf $(date +%Y%m%d-%T)_ClientLogs.tar.gz ./rcilog
		echo "Done"	
		running=0
	else
		percent=$((100*$files/$ClientCount))
		logmsg="Es laufen noch [$files/$ClientCount] Installationen. ($percent%)"
		echo $logmsg; #logit	
		sleep 2	
	fi
done